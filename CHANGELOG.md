# Changelog


Todas as mudanças significativas neste projeto serão documentados neste arquivo.
O formato deste arquivo foi baseado no [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added
- Guia de Contribuição do projeto

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
- LICENSE, foi mudado o tipo .txt para md, visando o reconhecimento da licensa pelo Gitlab.

### Security
-